#!/bin/sh
# TODO: create backups
_path="$(realpath "$0")"
_dir="$(dirname "$_path")"

printf "Installing zplugin...\n"
mkdir -p "$HOME/.zplugin"
if [ ! -d "$HOME/.zplugin/bin" ]
then
    git clone https://github.com/zdharma/zplugin.git "$HOME/.zplugin/bin"
    printf "\e[32m...done.\e[0m\n"
else
    printf "\e[32m...zplugin already installed.\e[0m\n"
    printf "Updating zplugin...\n"
    (cd "$HOME/.zplugin/bin" && git pull)
    printf "\e[32m...done.\e[0m\n"
fi

_common_dir="$(realpath "$_dir/../shell-common")"

printf "Linking aliases...\n"
mkdir -p "$HOME/.aliases.d"
ln -sf "$_common_dir/aliases" "$HOME/.aliases"
printf "\e[32m...done.\e[0m\n"

printf "Linking functions...\n"
mkdir -p "$HOME/.functions.d"
ln -sf "$_common_dir/functions" "$HOME/.functions"
printf "\e[32m...done.\e[0m\n"

printf "Generating completions for...\n"
_compl_dir="$HOME/.zsh-completions"
mkdir -p "$_compl_dir"
if command -v pip > /dev/null
then
    printf "* pip\n"
    pip completion --zsh > "$_compl_dir/pip"
fi
if command -v lab > /dev/null
then
    printf "* lab\n"
    lab completion zsh > "$_compl_dir/_lab"
fi
printf "\e[32m...done.\e[0m\n"

printf "Linking zshrc...\n"
ln -sf "$_dir/zshrc" "$HOME/.zshrc"
printf "\e[32m...done.\e[0m\n"
