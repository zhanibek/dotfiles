#!/bin/sh
# TODO: create backups
_path="$(realpath "$0")"
_dir="$(dirname "$_path")"
_tmpdir=/tmp/vim-plug

printf "Installing vim-plug...\n"
mkdir -p "$HOME/.vim/autoload"
git clone https://github.com/junegunn/vim-plug.git $_tmpdir
mv "$_tmpdir/plug.vim" "$HOME/.vim/autoload"
printf "\e[32m...done\e[0m\n"

printf "Linking vimrc...\n"
ln -sf "$_dir/vimrc" "$HOME/.vimrc"
printf "\e[32m...done\e[0m\n"

printf "Installing NeoVim config...\n"
_nv_config_dir="$HOME/.config/nvim"
if [ -n "$XDG_CONFIG_HOME" ]
then
    _nv_config_dir="$XDG_CONFIG_HOME/nvim"
fi
mkdir -p "$_nv_config_dir"
ln -sf "$_dir/init.vim" "$_nv_config_dir/init.vim"
printf "\e[32m...done\e[0m\n"
